// Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));


// Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

// const toDolist = [];
// toDolist.push(title());

// function title(item){
// 	return fetch('https://jsonplaceholder.typicode.com/todos');
// }

fetch('https://jsonplaceholder.typicode.com/todos')
    .then(response => response.json())
    .then(json => {
        json.map((item) => {
        console.log(item.title) 
  })
})


// Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));


// Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response) => response.json())
	.then((json) => console.log(
		`The item "${json.title}" has a status of "${json.completed}"`
    ));

// Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	header: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World!',
		userID: 1
	}),
})
.then((response) => response.json())
.then((json) => console.log(json));

// Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		body: 'Hello again!',
		id: 1,
		title: 'Updated post',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/* Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID
*/


fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'PUT',
    headers: {
        'Content-type': 'application/json'
    },
    body: JSON.stringify({
    	title: "Updated To Do List Item",
        description: 'To update my to do list with a different data structure',
        status: "Pending",
        dateCompleted: "Pending",
        userId: 1
        
    }),
})
.then((response) => response.json())
.then((json) => console.log(json));


// Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
})

